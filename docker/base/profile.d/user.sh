#!/usr/bin/env sh

if [ -n "${GID}" ] && [ -n "${GROUP}" ] && [ "$(id -g)" != "${GID}" ]
then
  sudo groupmod --gid "${GID}" "${GROUP}"
fi

if [ -n "${UID}" ] && [ -n "${USER}" ] && [ "$(id -u)" != "${UID}" ]
then
  sudo usermod --uid "${UID}" "${USER}"
fi
