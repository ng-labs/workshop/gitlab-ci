#!/usr/bin/env sh

if [ -n "${APP_ENV}" ] && [ "${APP_ENV}" != "dev" ]
then
  exit 0
fi

if [ -n "${FTP_PASSWORD}" ] || [ -n "${FTP_USER}" ] || [ -n "${FTP_HOST}" ]
then
  sshpass -p "${FTP_PASSWORD}" ssh -o StrictHostKeyChecking=no "${FTP_USER}@${FTP_HOST}" || true
fi

exec "$@"
