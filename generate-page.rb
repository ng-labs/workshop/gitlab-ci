require 'erb'

@template = ARGV[0]
@output = ARGV[1]

@public_dir = ENV['PUBLIC_DIR']

@title = ENV['CI_BRANCH_NAME']

## Docker
@docker_reports = {}

def has_docker_report()
    File.directory?(@public_dir + '/docker')
end

if has_docker_report()
    Dir.foreach(@public_dir + '/docker') do |image_name|
      next if image_name == '.' or image_name == '..'
      if File.directory?(@public_dir + '/docker/' + image_name)
        Dir.foreach(@public_dir + '/docker/' + image_name) do |report_name|
        next if report_name == '.' or report_name == '..'
            @report = {}
            @report[report_name] = @public_dir + '/docker/' + image_name + '/' + image_name
            @docker_reports[image_name] = @report
        end
      end
    end
end

## Symfony
@symfony_reports = {}

def has_symfony_report()
    File.directory?(@public_dir + '/symfony')
end

if has_symfony_report()
    Dir.foreach(@public_dir + '/symfony') do |report|
      next if report == '.' or report == '..'
      if File.directory?(@public_dir + '/symfony/' + report)
        @symfony_reports[report + ' coverage report'] = @public_dir + '/symfony/' + report
      else
        @symfony_reports[report] = @public_dir + '/symfony/' + report
      end
    end
end

## VueJs
@vuejs_reports = {}

def has_vuejs_report()
    File.directory?(@public_dir + '/vuejs')
end

if has_vuejs_report()
    Dir.foreach(@public_dir + '/vuejs') do |report|
      next if report == '.' or report == '..'
      if File.directory?(@public_dir + '/vuejs/' + report)
        @vuejs_reports[report + ' coverage report'] = @public_dir + '/symfony/' + report
      else
        @vuejs_reports[report] = @public_dir + '/vuejs/' + report
      end
    end
end

## Cypress
@cypress_resources = {}

if File.directory?(@public_dir + '/vuejs/cypress/resources/videos')
    Dir.foreach(@public_dir + '/vuejs/cypress/resources/videos') do |video|
      next if video == '.' or video == '..'
      @video_resources = {}
      @video_resources[video] = @public_dir + '/vuejs/cypress/resources/videos/' + video
      @cypress_resources['cypress'] = @video_resources
    end
end

if File.directory?(@public_dir + '/vuejs/cypress/resources/screenshots')
    Dir.foreach(@public_dir + '/vuejs/cypress/resources/screenshots') do |screenshots|
      next if screenshots == '.' or screenshots == '..'
      @screenshot = {}

      if File.directory?('./tests/e2e/screenshots/' + screenshots)
        Dir.foreach('./tests/e2e/screenshots/' + screenshots) do |value|
            next if value == '.' or value == '..'
            @screenshot[value] = @public_dir + '/vuejs/cypress/resources/screenshots/' + screenshots + '/' + value
        end
      else
        @screenshot[screenshots] = @public_dir + '/vuejs/cypress/resources/screenshots/' + screenshots
      end

      @cypress_resources['screenshots'] = @screenshot
    end
end

rhtml = ERB.new(File.read(@template))

File.open(@output, 'w') do |f|
  f.write rhtml.result(binding)
end
